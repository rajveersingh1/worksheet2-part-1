import Morse
#Basic Node
tree = Morse.Node("START")
tree.buildtree()    #building Morse Tree
e = Morse.encode(tree,"Good Morning") #Calling encode Function of morse.py
print(e)    #printing the output of encode function
print(Morse.decode(tree,e)) #calling decode function and printing decode output