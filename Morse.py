class Node:
    def __init__(self, value, dot=None, dash=None):
        self.value = value
        self.dot = dot
        self.dash = dash

    def insert(self,value,pos,str = "inserted successfully"):
        temp = self
        pos_array = [char for char in pos]
        for i in pos_array:
            if i == '.':
                if temp.dot== None:
                    temp.dot = Node("")
                temp = temp.dot
            else:
                if temp.dash == None:
                    temp.dash = Node("")
                temp = temp.dash
        if temp.value != '':
            str = "Already Available"
            return str
        temp.value = value  
        return str
    def deletenodewithchar(self, node):
        val = self.deletenodewithchar1(node)
        if val == 1:
            return "Success"
        else:
            return "node not available"
    def deletewithcode(self,node):
        temp = self
        pos_array = [char for char in node]
        for i in pos_array:
            if i == '.':
                if temp.dot== None:
                    return "Node not Available"
                temp = temp.dot
            else:
                if temp.dash == None:
                    return "Node not Available"
                temp = temp.dash
        deletenode(temp)
        return "Node deleted Successfully"


    def deletenodewithchar1(self, node):
        tree = self
        val1 = val2 = 0
        if tree.value != node:
            if tree.dot != None:
                val1 = tree.dot.deletenodewithchar1(node)
            if tree.dash != None:
                val2 = tree.dash.deletenodewithchar1(node)
            if val1 == 1 or val2 == 1:
                return 1
            else:
                return 0
        else:
            deletenode(tree)
            return 1
        
    def printtree(self,val = 0):
        #printing the Morse Code in Binary Tree formation
        str = ""
        for i in range(0,val):
            str = str +'\t'
        print(str + self.value)
        val = val + 1
        if self.dot != None:
            self.dot.printtree(val)
        if self.dash != None:
            self.dash.printtree(val)
    
    def buildtree(self):
        #buiilding the basic Morse binary tree
        tree = self
        #Level 1
        tree.insert("E",".")
        tree.insert("T","-")

        #Level 2
        tree.insert("I","..")
        tree.insert("A",".-")
        tree.insert("N","-.")
        tree.insert("M","--")

        #Level 3
        tree.insert("S","...")
        tree.insert("U","..-")
        tree.insert("R",".-.")
        tree.insert("W",".--")
        tree.insert("D","-..")
        tree.insert("K","-.-")
        tree.insert("G","--.")
        tree.insert("O","---")

        #Level 4
        tree.insert("H","....")
        tree.insert("V","...-")
        tree.insert("F","..-.")
        tree.insert("L",".-..")
        tree.insert("P",".--.")
        tree.insert("J",".---")
        tree.insert("B","-...")
        tree.insert("X","-..-")
        tree.insert("C","-.-.")
        tree.insert("Y","-.--")
        tree.insert("Z","--..")
        tree.insert("Q","--.-")

        #Level 5
        tree.insert("5",".....")
        tree.insert("4","....-")
        tree.insert("3","...--")
        tree.insert("2","..---")
        tree.insert("+",".-.-.")
        tree.insert("1",".----")
        tree.insert("6","-....")
        tree.insert("=","-...-")
        tree.insert("/","-..-.")
        tree.insert("7","--...")
        tree.insert("8","---..")
        tree.insert("9","----.")
        tree.insert("0","-----")
    
        #Advanced Nodes for tast 4
        tree.insert(".",".-.-.-")
        tree.insert("(","-.--.")
        tree.insert(",", "--..--")
        tree.insert(")", "-.--.-")
        tree.insert(".", "-....-")
        tree.insert("?", "..--.")
        tree.insert("&", ".-...")
        tree.insert("_", "..--.-")
        tree.insert("'", "----.")
        tree.insert(":", "---...")
        tree.insert("\"", ".-..-.")
        tree.insert("!", "-.-.--")
        tree.insert("$", "...-..-")
        tree.insert(";", "-.-.-.")



def getMorseCode( node, character, code):
    if node==None:
        return False
    elif node.value==character:
        return True
    else:  
        if getMorseCode(node.dot,character,code)==True:
            code.insert(0,".")
            return True
        elif getMorseCode(node.dash,character,code)==True: 
            code.insert(0,"-")
            return True

                    
      
#Let's initialise our binary tree:
def encode(tree, message):
    morseCode = ""
    message = message.upper()
    #Convert the message, one character at a time!
    for character in message:
        dotsdashes = []
        getMorseCode(tree,character,dotsdashes)
        code = "".join(dotsdashes)
        if code == '' and character != ' ':
            return "error node of this symbol not available in tree"
        morseCode = morseCode + code + " "
    morseCode = morseCode[:-1]
    return morseCode

def decode(tree, message):
    #Message Input
    #message = input("Enter a morsecode to convert into Massage:")
    splitmessage = message.split()
    mas=""
    for val in splitmessage:
        to_array = [char for char in val]
        node1 = tree
        for i in to_array:
            if i =='.':
                node1 = node1.dot
                if node1 == None:
                    return "error node of this symbol not available in tree"
            elif i == '-':
                node1 = node1.dash
                if node1 == None:
                    return "error node of this symbol not available in tree"
        if node1.value == '':
            return "Value of this node is empty"
        code = "".join(node1.value)
        mas = mas + code
    return mas

def deletenode(tree):
    if tree.dot != None:
        deletenode(tree.dot)
    elif tree.dash != None:
        deletenode(tree.dash)
    tree = None


