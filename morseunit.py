import Morse
import unittest
  
class TestMorse(unittest.TestCase):
    # 5 test cases of encode
    #Characters are in Capital letters
    def test_encode_US(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.encode(tree,"US"), "..- ...")
    #Characters are in lower letters
    def test_encode_us(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.encode(tree,"us"), "..- ...")
    #Characters with space
    def test_encode_twoWords(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.encode(tree,"Good Morning"), "--. --- --- -..  -- --- .-. -. .. -. --.")
    # All characters are not available
    def test_encode_WrongWord(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.encode(tree,"<>"), "error node of this symbol not available in tree")
    # Single characters are not available
    def test_encode_WrongChar(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.encode(tree,"A{}"), "error node of this symbol not available in tree")    
    
    #Test Cases of Decode
    #Test case with uppar case
    def test_decode_US(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.decode(tree,Morse.encode(tree,"US")), "US")
    #Testcase in lower letters
    def test_decode_us(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.decode(tree,Morse.encode(tree,"us")), "us")
    #Characters with space
    def test_decode_twoWords(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.decode(tree,Morse.encode(tree,"Good Morning")), "Good Morning")
    #Node not avaialble
    def test_decode_WrongChar(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.decode(tree,"......."), "error node of this symbol not available in tree")
    #Empty node
    def test_decode_WrongChar(self):
        tree = Morse.Node("START")
        tree.buildtree() 
        self.assertEqual(Morse.decode(tree,"..--"), "Value of this node is empty")    

    #Tree tests
    #Empty Tree
    def test_emptytree(self):
        tree = Morse.Node("START")
        self.assertEqual(tree.dot == None and tree.dash == None, True)

    #not empty tree
    def test_notemptytree(self):
        tree = Morse.Node("START")
        tree.insert("E",".")
        self.assertEqual(tree.dot != None or tree.dash != None, True)
    
    #not allow to override previous value
    def test_overridenode(self):
        tree = Morse.Node("START")
        tree.insert("E",".")
        self.assertEqual(tree.insert("E","."),"Already Available")
    
    #delete node successfully with character
    def test_deletesuccessfully(self):
        tree = Morse.Node("START")
        tree.insert("E",".")
        self.assertEqual(tree.deletenodewithchar("E"),"Success")

    #delete node if not available with character
    def test_deletenotsuccessfully(self):
        tree = Morse.Node("START")
        tree.insert("E",".")
        self.assertEqual(tree.deletenodewithchar("T"),"node not available")
    
        #delete node successfully with code
    def test_deletesuccessfully(self):
        tree = Morse.Node("START")
        tree.insert("E",".")
        self.assertEqual(tree.deletewithcode("."),"Node deleted Successfully")

    #delete node if not available with code
    def test_deletenotsuccessfully(self):
        tree = Morse.Node("START")
        tree.insert("E",".")
        self.assertEqual(tree.deletewithcode("-"),"Node not Available")


if __name__ == "__main__":
    unittest.main()
